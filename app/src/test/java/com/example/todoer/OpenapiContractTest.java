package com.example.todoer;

import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.parser.OpenAPIV3Parser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings({"unchecked", "rawtypes"})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class OpenapiContractTest {

    @Value("${local.server.port}") int port;

    @Test
    public void shouldFulfillOpenapiContract() throws IOException {
        var openApiParser = new OpenAPIV3Parser();

        OpenAPI contractApi = openApiParser.read("/openapi.contract.yaml").servers(emptyList());
        OpenAPI actualApi = openApiParser.read("http://localhost:" + port + "/v3/api-docs").servers(emptyList());

        Yaml.mapper().writeValue(new File("target/openapi.actual.yaml"), actualApi);

        //so let's transform OpenApi->yaml->map and compare resulting maps:
        var yamlParser = new org.yaml.snakeyaml.Yaml();
        Map<String, Object> contractApiAsMap = yamlParser.load(Yaml.mapper().writeValueAsString(contractApi));
        Map<String, Object> actualApiAsMap = yamlParser.load(Yaml.mapper().writeValueAsString(actualApi));
        compareRecursively("$", contractApiAsMap, actualApiAsMap);

        //extra check if my map-compare is buggy (awful output though):
        assertThat(actualApiAsMap)
             .usingRecursiveComparison()
             .isEqualTo(contractApiAsMap);
    }

    private void compareRecursively(String location, Map<String, Object> expected, Map<String, Object> actual) {
        assertKeysAreTheSame(location, expected, actual);
        expected.forEach((key, expectedValue) -> {
            var actualValue = actual.get(key);
            if(expectedValue instanceof Map && actualValue instanceof Map)
                compareRecursively(location+"."+key, (Map)expectedValue, (Map)actualValue);
            else
                assertThat(actualValue)
                        .describedAs(location+"."+key+" must be the same")
                        .isEqualTo(expectedValue);
        });
    }

    private void assertKeysAreTheSame(String location, Map<String, Object> expected, Map<String, Object> actual) {
        assertThat(expected.keySet())
                .describedAs("Children of " + location + " must be the same")
                .containsExactlyInAnyOrder(actual.keySet().toArray(new String[0]));
    }
}
