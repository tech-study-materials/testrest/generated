package com.example.todoer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.api.TodoerApi;
import org.openapitools.client.model.NewTodoRequest;
import org.openapitools.client.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class GeneratedClientTests {

	@Autowired Repo repo;
	@Value("${local.server.port}") int port;

	TodoerApi api;

	@BeforeEach
	public void setup() {
		api = new TodoerApi(new ApiClient()
				.setPort(port));

		repo.deleteAll();
	}

	@Test
	void testCreateAndGet() throws ApiException {
		//given
		api.createTodo("adam", new NewTodoRequest().description("buy milk"));

		//when
		var todos = api.getTodos("adam");

		//then
		assertThat(todos)
				.usingElementComparatorIgnoringFields("id")
				.containsExactlyInAnyOrder(
						new Todo().user("adam").description("buy milk")
				);
	}

	@Test
	void testMarkDone() throws ApiException {
		//given
		long buyMilkId = api
				.createTodo("adam", new NewTodoRequest().description("buy milk"))
				.getId();

		//when
		api.markDone("adam", buyMilkId);
		var todos = api.getTodos("adam");

		//then
		assertThat(todos).isEmpty();
	}
}
